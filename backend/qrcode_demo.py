import qrcode_terminal
import json

with open('../config.json') as json_file:
    data = json.load(json_file)
    ip = data['ip']
    port = data['port']

print("***** for translate model *****")
qrcode_terminal.draw('http://%s:%s/api/helmet_demo_trans/' % (ip, port))
print("***** for inference *****")
qrcode_terminal.draw('http://%s:%s/api/helmet_demo_inference/' % (ip, port))