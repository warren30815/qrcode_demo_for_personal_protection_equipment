#!/bin/bash
# run this with "source install.sh"
sudo killall apt apt-get
sudo rm /var/lib/apt/lists/lock
sudo rm /var/cache/apt/archives/lock
sudo rm /var/lib/dpkg/lock*
sudo dpkg --configure -a
sudo apt-get update
sudo apt-get install python3-pip -y
sudo apt-get install jq -y
sudo apt-get install python-opencv -y
sudo apt-get install python3-opencv -y
sudo -H pip3 install --upgrade pip
sudo pip3 install -r requirements.txt
wget https://apt.repos.intel.com/openvino/2020/GPG-PUB-KEY-INTEL-OPENVINO-2020
sudo apt-key add GPG-PUB-KEY-INTEL-OPENVINO-2020
sudo sh -c "echo \"deb https://apt.repos.intel.com/openvino/2020 all main\" >> /etc/apt/sources.list.d/intel-openvino-2020.list"
sudo apt-get update
sudo apt-get install intel-openvino-dev-ubuntu18-2020.4.287 -y