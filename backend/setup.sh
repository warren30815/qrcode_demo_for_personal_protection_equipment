#!/bin/bash
# usage: source setup.sh
if ! [ -d "safety-gear-detector-python" ]; then
    git clone https://github.com/intel-iot-devkit/safety-gear-detector-python.git
fi

if ! [ -d "safety-gear-detector-python/resources/worker-safety-mobilenet/FP32" ]; then
    cd safety-gear-detector-python
	source /opt/intel/openvino/bin/setupvars.sh
	./setup.sh
	cd ..
fi

cp -f safety_gear_detector.py safety-gear-detector-python/application
cp -f inference.py safety-gear-detector-python/application
rm safety-gear-detector-python/resources/*.mp4 2> /dev/null
echo "All done"