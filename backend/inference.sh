#!/bin/bash
FilePath=$1
cd safety-gear-detector-python/application
source /opt/intel/openvino/bin/setupvars.sh
./safety_gear_detector.py -d CPU -m /opt/intel/openvino/deployment_tools/open_model_zoo/tools/downloader/intel/person-detection-retail-0013/FP32/person-detection-retail-0013.xml -sm ../resources/worker-safety-mobilenet/FP32/worker_safety_mobilenet.xml --img $FilePath
