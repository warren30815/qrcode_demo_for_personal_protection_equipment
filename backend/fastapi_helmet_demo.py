from fastapi import FastAPI
from fastapi.responses import JSONResponse
import subprocess
import os
from fastapi import UploadFile, File, Form
import cv2
from io import BytesIO
import numpy as np 
import base64

app = FastAPI()

@app.get("/api/helmet_demo_trans")
async def helmet_trans():
	process = subprocess.Popen(['/bin/bash', 'setup.sh'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	content = {"message": stdout.decode("utf-8"), "error": stderr.decode("utf-8")}
	return JSONResponse(content=content)

@app.post("/api/helmet_demo_inference")
async def helmet_demo(model_name: str = Form(...), file: UploadFile = File(...)):
	# print(file.filename)
	filename = "tmp123.jpg"
	output_filepath = "safety-gear-detector-python/application/output.jpg"
	img_str = file.file.read()
	nparr = np.fromstring(img_str, np.uint8)
	img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
	if os.path.isfile(filename):
		os.remove(filename)
	cv2.imwrite(filename, img_np) 
	os.system("cp -f %s safety-gear-detector-python/application" % (filename))
	process = subprocess.Popen(['/bin/bash', 'inference.sh', filename], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()

	with open(output_filepath, mode='rb') as file:
		output_img = file.read()
		jpg_base64_string = base64.encodebytes(output_img).decode("utf-8")

	image_data = base64.b64decode(jpg_base64_string)
	with open("check.jpg", 'wb') as jpg_file:
		jpg_file.write(image_data)
	content = {"message": stdout.decode("utf-8"), "error": stderr.decode("utf-8"), "infer_result": jpg_base64_string}
	return JSONResponse(content=content)
