# Personal Protection Equipment Detection demo
**Frontend**: iOS (Swift)

**Backend**: FastAPI (Python) + Uvicorn (Web Server)

**AI**: Intel Openvino AI toolkit

Backend will generate QRCode, then user can use APP to scan QRCode, and upload image to server to infer whether workers wear the safety vest or helmet.

Below is the demo video (on Youtube)

[![](https://img.youtube.com/vi/IHM6pDYEXDw/0.jpg)](https://www.youtube.com/shorts/IHM6pDYEXDw)
