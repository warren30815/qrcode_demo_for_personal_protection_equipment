//
//  ViewController.swift
//  qrcode_test
//
//  Created by 許竣翔 on 2020/8/16.
//  Copyright © 2020 許竣翔. All rights reserved.
//

import UIKit
import MercariQRScanner
import Alamofire

var url = ""

struct ByteEncoding: ParameterEncoding {
  private let data: Data

  init(data: Data) {
    self.data = data
  }

  func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
    var urlRequest = try urlRequest.asURLRequest()
    urlRequest.httpBody = data
    return urlRequest
  }
}

class ViewController: UIViewController, QRScannerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var resultimageView: UIImageView!
    
    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError) {
        print(error)
    }

    func qrScannerView(_ qrScannerView: QRScannerView, didSuccess code: String) {
        qrScannerView.removeFromSuperview()
        url = code
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true)
    }
    
    // MARK: - Image picker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        /// 選中一張圖片時觸發這個方法，返回關於選中圖片的 info
        /// 獲取這張圖片中的 originImage 屬性，就是圖片本身
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError("error: did not picked a photo")
        }
        
        /// 選中圖片以後關閉 picker controller
        picker.dismiss(animated: true) { [unowned self] in
            // add a image view on self.view
            self.imageView.image = selectedImage
        }
        
        print(url)
        
        let imageData = selectedImage.jpegData(compressionQuality: 1.0)
        let parameters = ["model_name": "helmet_demo_model"] as [String : Any]
        let headers: HTTPHeaders = ["Content-type": "multipart/form-data"]
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
            if (imageData != nil){
                // !!! withName: Form key, can't assign arbitrarily
                multipartFormData.append(imageData!, withName: "file", fileName: "test.jpg", mimeType: "image/jpg")
            }
        }, to: url, method: .post, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                switch response.result {
                    case .success(let value):
                        if let JSON = value as? [String: Any] {
                            var imageBase64String = JSON["infer_result"] as! String
                            imageBase64String = String(imageBase64String.filter { !" \n\t\r".contains($0) })
                            let result_img_data = Data(base64Encoded: imageBase64String)
                            let result_img = UIImage(data: result_img_data!)
                            self.resultimageView.image = result_img
                        }
                    case .failure(let error):
                        print(error)
                        break
                }
            } else {
                print("Failed, status code:", response.response?.statusCode ?? 500)
                print("Error in upload: \(String(describing: response.error?.localizedDescription))")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
        let qrScannerView = QRScannerView(frame: view.bounds)
        view.addSubview(qrScannerView)
        qrScannerView.configure(delegate: self)
        qrScannerView.startRunning()
    }
}
